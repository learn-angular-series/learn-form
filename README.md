## learn-form

此工程用来示范 Angular Form 的用法。

分支上的内容：

1. quick-start分支：快速上手
1. two-way-data-binding：双向数据绑定
1. validation：表单校验基础

响应式表单：代码比较复杂，请参见 NiceFish 里面的 user-register.component.ts 组件。

动态表单：请参见 NiceFish 里面的 user-profile.component.ts 组件。

## 备注

learn-anguar-* 是一个Angular系列教学项目用的实例源代码，一共分成了 16 个项目，共 150 个分支。

- 国内推荐使用gitee.com，链接在这里：https://gitee.com/organizations/learn-angular-series
- github上的链接在这里：https://github.com/learn-angular-series
- 如果你需要一个更复杂、更完整一点的例子，请试试NiceFish：https://gitee.com/mumu-osc/NiceFish
- 如果你需要其它形式的帮助，请给我发邮件，或者直接联系我。
- 这是一个教学项目，请勿用于商业用途。